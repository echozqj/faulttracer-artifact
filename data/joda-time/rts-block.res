[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 12 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f49c671d9926eb677490a76cc8317970e5be433c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: .
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 4164, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 10.491 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 124/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/f49c671d9926eb677490a76cc8317970e5be433c
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 124

Results :

Tests run: 4164, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 13.657 s
[INFO] Finished at: 2017-02-21T15:48:00-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f49c671d9926eb677490a76cc8317970e5be433c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler$Rule:<init>:(Ljava/util/StringTokenizer;)V:0
[FaultTracer] RTS excluded 123 out of 124 test classes using 687ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 11, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.368 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c

Results :

Tests run: 11, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.342 s
[INFO] Finished at: 2017-02-21T15:48:08-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f49c671d9926eb677490a76cc8317970e5be433c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler$Rule:<init>:(Ljava/util/StringTokenizer;)V:0
[FaultTracer] RTS excluded 123 out of 124 test classes using 678ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 11, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.876 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 1

Results :

Tests run: 11, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.156 s
[INFO] Finished at: 2017-02-21T15:48:13-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 671ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.25 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.353 s
[INFO] Finished at: 2017-02-21T15:48:22-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/783e0c7552a1b7d38dc32334a75e9bb73f07575c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 656ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.697 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.023 s
[INFO] Finished at: 2017-02-21T15:48:27-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 12 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 846ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.284 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.583 s
[INFO] Finished at: 2017-02-21T15:48:36-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 12 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/98aa441a6b110643665896ab9dac7cab6ee88936
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 647ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.718 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.354 s
[INFO] Finished at: 2017-02-21T15:48:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 1929ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.739 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.494 s
[INFO] Finished at: 2017-02-21T15:48:58-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4bac3c38ead34a77954e9901c86f691366da723d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 1570ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.582 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.301 s
[INFO] Finished at: 2017-02-21T15:49:13-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 1668ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.714 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.458 s
[INFO] Finished at: 2017-02-21T15:49:42-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/6265a7522991ce56b67f1a4935003bcaf029b1e8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 1925ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.694 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.826 s
[INFO] Finished at: 2017-02-21T15:49:58-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 1 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler$Rule:<init>:(Ljava/util/StringTokenizer;)V:0
[FaultTracer] RTS excluded 123 out of 124 test classes using 1830ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 12, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.915 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b

Results :

Tests run: 12, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.748 s
[INFO] Finished at: 2017-02-21T15:50:26-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d2a0bb0593dab948c9b738266be880eaf00352df
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 1 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler$Rule:<init>:(Ljava/util/StringTokenizer;)V:0
[FaultTracer] RTS excluded 123 out of 124 test classes using 761ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 12, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.983 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 1

Results :

Tests run: 12, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.998 s
[INFO] Finished at: 2017-02-21T15:50:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 977ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.309 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.030 s
[INFO] Finished at: 2017-02-21T15:50:44-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/451e2c4f87e38076f8b768a6f59b4eab96744f3b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 124 out of 124 test classes using 989ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.125 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.496 s
[INFO] Finished at: 2017-02-21T15:50:50-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/base/AbstractInterval:checkInterval:(JJ)V:1
[FaultTracer] RTS excluded 120 out of 124 test classes using 979ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 230, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.455 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 4/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a

Results :

Tests run: 230, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.941 s
[INFO] Finished at: 2017-02-21T15:51:02-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/dadca66121a67692d937f06c7df797886cf17d31
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/base/AbstractInterval:checkInterval:(JJ)V:1
[FaultTracer] RTS excluded 120 out of 124 test classes using 1216ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 230, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.098 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 4/124 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a
[FaultTracer] Test coverage set consistent with mvn test set: total 124, reran 4

Results :

Tests run: 230, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.423 s
[INFO] Finished at: 2017-02-21T15:51:12-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 1 9 0 0 0 0 0 0 4 0 0 1 2 7 5 0 0 0 0 0 1 0 0 1 1 5 3
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeParserBucket$SavedField:set:(JZ)J:1
[FaultTracer]  Basic block changes: org/joda/time/TestAll:suite:()Ljunit/framework/Test;:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(IIIIIIILorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(JLorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(Ljava/lang/Object;Lorg/joda/time/DateTimeZone;)V:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/ZonedChronology:localToUTC:(J)J:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(Ljava/lang/Object;Lorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateMidnightMillis:(III)J:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateTimeMillis:(IIII)J:2
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateTimeMillis:(IIIIIII)J:2
[FaultTracer] RTS excluded 38 out of 124 test classes using 1124ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 3367, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.274 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 87/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4

Results :

Tests run: 3367, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.681 s
[INFO] Finished at: 2017-02-21T15:51:29-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a357a84750ac43ca0d1a068099390eadbfc0f8a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 1 9 0 0 0 0 0 0 4 0 0 1 2 7 5 0 0 0 0 0 1 0 0 1 1 5 3
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeParserBucket$SavedField:set:(JZ)J:1
[FaultTracer]  Basic block changes: org/joda/time/TestAll:suite:()Ljunit/framework/Test;:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(IIIIIIILorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(JLorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(Ljava/lang/Object;Lorg/joda/time/DateTimeZone;)V:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/ZonedChronology:localToUTC:(J)J:0
[FaultTracer]  Basic block changes: org/joda/time/base/BaseDateTime:<init>:(Ljava/lang/Object;Lorg/joda/time/Chronology;)V:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateMidnightMillis:(III)J:0
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateTimeMillis:(IIII)J:2
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicChronology:getDateTimeMillis:(IIIIIII)J:2
[FaultTracer] RTS excluded 38 out of 124 test classes using 1001ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 3367, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 11.902 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 87/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 87

Results :

Tests run: 3367, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 16.479 s
[INFO] Finished at: 2017-02-21T15:51:46-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 831ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.261 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.226 s
[INFO] Finished at: 2017-02-21T15:51:57-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d7774f13ad2dc7cf7295bb8376c21bbf5c662fc4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 691ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.942 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.624 s
[INFO] Finished at: 2017-02-21T15:52:03-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 752ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.291 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.466 s
[INFO] Finished at: 2017-02-21T15:52:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d750c2bc808a0cff07ffeeb09d6af6be20754bdf
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 667ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.693 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.079 s
[INFO] Finished at: 2017-02-21T15:52:16-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 22 0 0 0 0 0 0 0 0 0 0 0 2
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$NumberFormatter:parseInto:(Lorg/joda/time/format/DateTimeParserBucket;Ljava/lang/CharSequence;I)I:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_date:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateElementParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_localDateParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDate:()V:0
[FaultTracer] RTS excluded 96 out of 125 test classes using 737ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 1207, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.815 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 29/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35

Results :

Tests run: 1207, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.116 s
[INFO] Finished at: 2017-02-21T15:52:26-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.8.3-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4289f5e53527bd20e241d5e92fe55ef0ef20576b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 22 0 0 0 0 0 0 0 0 0 0 0 2
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$NumberFormatter:parseInto:(Lorg/joda/time/format/DateTimeParserBucket;Ljava/lang/CharSequence;I)I:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_date:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicWeekDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_ordinalDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_dateElementParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_localDateParser:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicDate:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_basicOrdinalDateTimeNoMillis:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDateTime:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestISODateTimeFormatParsing:test_weekDate:()V:0
[FaultTracer] RTS excluded 96 out of 125 test classes using 985ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 1207, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.25 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 29/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 29

Results :

Tests run: 1207, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.296 s
[INFO] Finished at: 2017-02-21T15:52:33-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 664ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.238 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.234 s
[INFO] Finished at: 2017-02-21T15:52:42-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a201881f01bce85efece778345ebd60cf58ba35
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 660ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.831 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.132 s
[INFO] Finished at: 2017-02-21T15:52:47-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 676ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.279 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.272 s
[INFO] Finished at: 2017-02-21T15:52:55-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/8f41ec9713568111cf6b81736d97b46aeb281ace
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 751ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.728 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.709 s
[INFO] Finished at: 2017-02-21T15:53:01-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 897ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.348 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.426 s
[INFO] Finished at: 2017-02-21T15:53:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7d2a3bf80b95c19fe3b2e101d619e3091bd06d4f
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 868ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.229 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.643 s
[INFO] Finished at: 2017-02-21T15:53:18-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/chrono/ZonedChronology:localToUTC:(J)J:4
[FaultTracer] RTS excluded 60 out of 125 test classes using 744ms 
Running org.joda.time.TestAllPackages

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 2748, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.883 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 65/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca

Results :

Tests run: 2748, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.077 s
[INFO] Finished at: 2017-02-21T15:53:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/556d2a007e74e51771f841ecbffb5b82f748dec3
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/chrono/ZonedChronology:localToUTC:(J)J:4
[FaultTracer] RTS excluded 60 out of 125 test classes using 964ms 
Running org.joda.time.TestAllPackages

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 2748, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 7.369 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 65/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 65

Results :

Tests run: 2748, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.232 s
[INFO] Finished at: 2017-02-21T15:53:44-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 930ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.315 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.204 s
[INFO] Finished at: 2017-02-21T15:53:53-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c0cbd65f849645635110df3b719c7e9cc00104ca
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 692ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.889 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.231 s
[INFO] Finished at: 2017-02-21T15:53:58-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 124 out of 125 test classes using 980ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.501 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.653 s
[INFO] Finished at: 2017-02-21T15:54:09-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/73fd550faa7b37ef31dd246752394cbb5f5543da
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 124 out of 125 test classes using 1019ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.275 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.628 s
[INFO] Finished at: 2017-02-21T15:54:16-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 0 1 1 2 1 0 0 0 0 0 0 0 0 1 1 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 123 out of 125 test classes using 686ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 58, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.308 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5

Results :

Tests run: 58, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.976 s
[INFO] Finished at: 2017-02-21T15:54:27-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/76fa43737656d859f0943f5326cf2b8565c4b032
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 0 1 1 2 1 0 0 0 0 0 0 0 0 1 1 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 123 out of 125 test classes using 791ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 58, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.861 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 58, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.492 s
[INFO] Finished at: 2017-02-21T15:54:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 0 1 2 1 1 0 0 0 0 0 0 0 0 1 1 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 123 out of 125 test classes using 682ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 57, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.297 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2

Results :

Tests run: 57, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.269 s
[INFO] Finished at: 2017-02-21T15:54:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/635830e5d8b7ee7c99ffaf40251936a876ee2df5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 0 1 2 1 1 0 0 0 0 0 0 0 0 1 1 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseDataFile:(Ljava/io/BufferedReader;Z)V:26
[FaultTracer] RTS excluded 123 out of 125 test classes using 673ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 57, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.084 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 57, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.436 s
[INFO] Finished at: 2017-02-21T15:54:47-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 1 0 2 0 0 0 0 0 0 0 0 0 1 0 1 0
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 694ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 48, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.282 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a

Results :

Tests run: 48, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.971 s
[INFO] Finished at: 2017-02-21T15:54:57-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/9ced54cc82338351cffd18d44c3b0ab21624e4f2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 1 0 2 0 0 0 0 0 0 0 0 0 1 0 1 0
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder:csCompare:(Ljava/lang/CharSequence;ILjava/lang/String;)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 1384ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 48, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.109 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 48, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.747 s
[INFO] Finished at: 2017-02-21T15:55:06-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 2142ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.622 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.952 s
[INFO] Finished at: 2017-02-21T15:55:34-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f5bd8f5063258501c7fba6f49925625dfdd7799a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1656ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.715 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.670 s
[INFO] Finished at: 2017-02-21T15:55:50-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1958ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.638 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.539 s
[INFO] Finished at: 2017-02-21T15:56:18-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e0b01d44d113a7a1504f48f0ae924a57a01d90a2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1875ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.645 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.949 s
[INFO] Finished at: 2017-02-21T15:56:34-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 33 0 0 0 0 0 0 0 0 0 0 0 0 36 0 0 0 0 0 0 0 0 0 0 0 33
[FaultTracer]  Basic block changes: org/joda/time/TestSeconds:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYearMonth_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriodType:assertEqualsAfterSerialization:(Lorg/joda/time/PeriodType;)V:0
[FaultTracer]  Basic block changes: org/joda/time/TestHours:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMonths:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeFieldType:doSerialization:(Lorg/joda/time/DateTimeFieldType;)Lorg/joda/time/DateTimeFieldType;:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutableDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalDate_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDurationFieldType:doSerialization:(Lorg/joda/time/DurationFieldType;)Lorg/joda/time/DurationFieldType;:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testSerialization1:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutableInterval_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeComparator:testSerialization1:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestTimeOfDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestWeeks:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutablePeriod_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestPreciseDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriodType:assertSameAfterSerialization:(Lorg/joda/time/PeriodType;)V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDays:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestMillisDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestScaledDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPartial_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriod_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeComparator:testSerialization2:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDuration_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestInterval_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYearMonthDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestInstant_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMinutes:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMonthDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/tz/TestCachedDateTimeZone:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateMidnight_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testSerialization2:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYears:testSerialization:()V:0
[FaultTracer] RTS excluded 92 out of 125 test classes using 933ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 1403, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.696 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 33/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c

Results :

Tests run: 1403, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.211 s
[INFO] Finished at: 2017-02-21T15:56:53-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e0d6015d3d42330fe0e4ab0c8fa0330435228bde
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 33 0 0 0 0 0 0 0 0 0 0 0 0 36 0 0 0 0 0 0 0 0 0 0 0 33
[FaultTracer]  Basic block changes: org/joda/time/TestSeconds:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYearMonth_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriodType:assertEqualsAfterSerialization:(Lorg/joda/time/PeriodType;)V:0
[FaultTracer]  Basic block changes: org/joda/time/TestHours:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMonths:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeFieldType:doSerialization:(Lorg/joda/time/DateTimeFieldType;)Lorg/joda/time/DateTimeFieldType;:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutableDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalDate_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDurationFieldType:doSerialization:(Lorg/joda/time/DurationFieldType;)Lorg/joda/time/DurationFieldType;:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testSerialization1:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutableInterval_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeComparator:testSerialization1:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestTimeOfDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestWeeks:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMutablePeriod_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestLocalDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestPreciseDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriodType:assertSameAfterSerialization:(Lorg/joda/time/PeriodType;)V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDays:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestMillisDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/field/TestScaledDurationField:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPartial_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestPeriod_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTime_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeComparator:testSerialization2:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDuration_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestInterval_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYearMonthDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestInstant_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMinutes:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestMonthDay_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/tz/TestCachedDateTimeZone:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateMidnight_Basics:testSerialization:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testSerialization2:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestYears:testSerialization:()V:0
[FaultTracer] RTS excluded 92 out of 125 test classes using 1072ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 1403, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.506 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 33/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 33

Results :

Tests run: 1403, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.342 s
[INFO] Finished at: 2017-02-21T15:57:00-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 964ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.264 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.469 s
[INFO] Finished at: 2017-02-21T15:57:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0e82517a7f4a2e659143ce84ff1f4ea706e9a88c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 956ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.907 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.740 s
[INFO] Finished at: 2017-02-21T15:57:18-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1111ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.442 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.925 s
[INFO] Finished at: 2017-02-21T15:57:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f4152948bdf4efa3075d770190c48139b1341309
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1168ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.166 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.454 s
[INFO] Finished at: 2017-02-21T15:57:39-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1134ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.369 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.334 s
[INFO] Finished at: 2017-02-21T15:57:52-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b6b8ef051decf0d450146cbc87475b9f4dcc3de8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1011ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.922 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.014 s
[INFO] Finished at: 2017-02-21T15:57:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 973ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.268 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.815 s
[INFO] Finished at: 2017-02-21T15:58:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/91ad3ada2d9c7d246cf8ec75726b6d085255f751
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1079ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.057 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.066 s
[INFO] Finished at: 2017-02-21T15:58:18-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1148ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.407 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.861 s
[INFO] Finished at: 2017-02-21T15:58:31-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/83c7de55bd5af7ef3fb27573c69b4d33c4414c61
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1085ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.221 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.096 s
[INFO] Finished at: 2017-02-21T15:58:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1181ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.545 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.282 s
[INFO] Finished at: 2017-02-21T15:58:51-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/aaf4396ceb75f39aaa4083f3c96aabc822deeb9e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1178ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.441 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.561 s
[INFO] Finished at: 2017-02-21T15:58:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1232ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.616 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.742 s
[INFO] Finished at: 2017-02-21T15:59:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/60be421469dd85893978bfc645b41e94c63ba1b1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1156ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.012 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.985 s
[INFO] Finished at: 2017-02-21T15:59:24-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1241ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.491 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.161 s
[INFO] Finished at: 2017-02-21T15:59:39-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/474bf290ab6e2eb51cf041f50a9943ca5f262520
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1159ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.289 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.563 s
[INFO] Finished at: 2017-02-21T15:59:48-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1171ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.351 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.985 s
[INFO] Finished at: 2017-02-21T16:00:00-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/aaaa7432d1c23cd6462724cb2c460efe9ffb028a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1041ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.173 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.251 s
[INFO] Finished at: 2017-02-21T16:00:08-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 881ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.271 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.846 s
[INFO] Finished at: 2017-02-21T16:00:20-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2190b1b82f07ebaa864b2775186c238f3aeb8885
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 936ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.289 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.095 s
[INFO] Finished at: 2017-02-21T16:00:27-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1136ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.511 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.101 s
[INFO] Finished at: 2017-02-21T16:00:40-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 34 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/125389ced2b4d159e8a14545d564a9e39e8e193c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:writeTo:(Ljava/lang/String;Ljava/io/OutputStream;)V:2
[FaultTracer] RTS excluded 123 out of 125 test classes using 1082ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 18, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.584 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 18, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.261 s
[INFO] Finished at: 2017-02-21T16:00:48-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1404ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.432 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.170 s
[INFO] Finished at: 2017-02-21T16:01:03-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e7e6d78327d8c7d837e2a229d920b7e224504253
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1630ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.643 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.364 s
[INFO] Finished at: 2017-02-21T16:01:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 0
[FaultTracer] RTS excluded 124 out of 125 test classes using 1198ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 27, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.536 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908

Results :

Tests run: 27, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.122 s
[INFO] Finished at: 2017-02-21T16:01:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5abe1c4dfe19f27ad155fd08970a0308607779b2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 0
[FaultTracer] RTS excluded 124 out of 125 test classes using 1144ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 27, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.36 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 27, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.234 s
[INFO] Finished at: 2017-02-21T16:01:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1184ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.581 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.554 s
[INFO] Finished at: 2017-02-21T16:01:58-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d979bd86945ba316bace078fd2314a2751d0c908
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1422ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.44 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.021 s
[INFO] Finished at: 2017-02-21T16:02:08-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0
[FaultTracer] RTS excluded 124 out of 125 test classes using 861ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 49, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.401 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e

Results :

Tests run: 49, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.320 s
[INFO] Finished at: 2017-02-21T16:02:20-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/6c50ef551e56b01909b999eb2645996a9b670567
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0
[FaultTracer] RTS excluded 124 out of 125 test classes using 923ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 49, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.228 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 49, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.706 s
[INFO] Finished at: 2017-02-21T16:02:27-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 755ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.269 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.125 s
[INFO] Finished at: 2017-02-21T16:02:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0b80f3ebf6c752debb8dbea87aa729968dcfa68e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 962ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.315 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.729 s
[INFO] Finished at: 2017-02-21T16:02:45-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 939ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.242 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.312 s
[INFO] Finished at: 2017-02-21T16:02:56-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a77d85d9c7ea5a2b987f35e185e12c622c0d13a4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 825ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.146 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.661 s
[INFO] Finished at: 2017-02-21T16:03:03-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1003ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.393 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.442 s
[INFO] Finished at: 2017-02-21T16:03:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/625c9048aa5ec2d38b850172fa9f0700c8c217b4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 814ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.12 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.340 s
[INFO] Finished at: 2017-02-21T16:03:21-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 884ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.357 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.798 s
[INFO] Finished at: 2017-02-21T16:03:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5e830f1b5c277627799fb035907a7596140517e3
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1026ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.866 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.430 s
[INFO] Finished at: 2017-02-21T16:03:39-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 983ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.253 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.504 s
[INFO] Finished at: 2017-02-21T16:03:50-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b0cdaa7ea200481284d8a5f1ee7c5acf21a013b5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 999ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.006 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.523 s
[INFO] Finished at: 2017-02-21T16:03:57-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 1 0 0 0 0 0 2 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:setCurrentMillisSystem:()V:0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:<clinit>:()V:0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:setCurrentMillisOffset:(J)V:2
[FaultTracer] RTS excluded 48 out of 125 test classes using 1017ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 3066, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.11 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 77/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476

Results :

Tests run: 3066, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.352 s
[INFO] Finished at: 2017-02-21T16:04:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c55c81b0d6f297dda50532e1fd0e2bf779323d86
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 1 0 0 0 0 0 2 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:setCurrentMillisSystem:()V:0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:<clinit>:()V:0
[FaultTracer]  Basic block changes: org/joda/time/DateTimeUtils:setCurrentMillisOffset:(J)V:2
[FaultTracer] RTS excluded 48 out of 125 test classes using 829ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 3066, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 11.143 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 77/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 77

Results :

Tests run: 3066, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 15.289 s
[INFO] Finished at: 2017-02-21T16:04:30-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 939ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.34 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.342 s
[INFO] Finished at: 2017-02-21T16:04:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/da91f20e8c7e7d4aa624e6260c534757168cd476
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 835ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.2 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.251 s
[INFO] Finished at: 2017-02-21T16:04:48-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 990ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.361 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.761 s
[INFO] Finished at: 2017-02-21T16:04:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/84aeb139de0149dd13a022120890985df5f1080a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 988ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.154 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.509 s
[INFO] Finished at: 2017-02-21T16:05:06-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 939ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.316 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.243 s
[INFO] Finished at: 2017-02-21T16:05:17-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5484d6ff6e722bd3993c468595c5b5c60b800930
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 968ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.173 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.543 s
[INFO] Finished at: 2017-02-21T16:05:23-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 977ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.305 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.330 s
[INFO] Finished at: 2017-02-21T16:05:34-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 13 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/40835fb3573ecdda5da26b100095894e131629b1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 944ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.045 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.656 s
[INFO] Finished at: 2017-02-21T16:05:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 798ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.297 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.280 s
[INFO] Finished at: 2017-02-21T16:05:52-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/19999da2cfb0cbc3af076ef984ea8a7b596f80f2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 965ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.534 s
[INFO] Finished at: 2017-02-21T16:05:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 777ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.222 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.212 s
[INFO] Finished at: 2017-02-21T16:06:10-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a8722f8427361ce564aefbcc3f6a82899ee55736
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 665ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.706 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.993 s
[INFO] Finished at: 2017-02-21T16:06:15-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1764ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.65 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.541 s
[INFO] Finished at: 2017-02-21T16:06:33-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0a91bbf06df0bee056261d4c52612ae12a661c56
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1604ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.814 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.989 s
[INFO] Finished at: 2017-02-21T16:06:48-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1786ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.631 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.619 s
[INFO] Finished at: 2017-02-21T16:07:16-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/88d97b818d9bbdaee06c3ed174c4fb6902d5c25d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1851ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.978 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 13.037 s
[INFO] Finished at: 2017-02-21T16:07:33-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicMonthOfYearDateTimeField:add:(JI)J:2
[FaultTracer] RTS excluded 53 out of 125 test classes using 688ms 
Running org.joda.time.TestAllPackages

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 2995, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.123 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 72/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0

Results :

Tests run: 2995, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.415 s
[INFO] Finished at: 2017-02-21T16:08:02-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/fef3ce069e7ef19965fc70d743bbaaa246897700
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/chrono/BasicMonthOfYearDateTimeField:add:(JI)J:2
[FaultTracer] RTS excluded 53 out of 125 test classes using 678ms 
Running org.joda.time.TestAllPackages

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 2995, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 7.178 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 72/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 72

Results :

Tests run: 2995, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.597 s
[INFO] Finished at: 2017-02-21T16:08:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 904ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.318 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.940 s
[INFO] Finished at: 2017-02-21T16:08:24-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.2
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/3195c2cad51f89a59041dae3130ec2d8e0546be0
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 655ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.647 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.850 s
[INFO] Finished at: 2017-02-21T16:08:29-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 696ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.343 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.481 s
[INFO] Finished at: 2017-02-21T16:08:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/72c59900d0f05257be01899b9353653ce481b032
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 690ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.06 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.404 s
[INFO] Finished at: 2017-02-21T16:08:43-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 923ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.325 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.191 s
[INFO] Finished at: 2017-02-21T16:08:54-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/a2e141279d34c29090c80a84c6a47ec33a90ed04
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 750ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.758 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.306 s
[INFO] Finished at: 2017-02-21T16:08:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 688ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.254 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.656 s
[INFO] Finished at: 2017-02-21T16:09:09-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/719ee5ce890a6df4abb981591aef15519c07f6ba
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 675ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.783 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.062 s
[INFO] Finished at: 2017-02-21T16:09:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 679ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.268 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.606 s
[INFO] Finished at: 2017-02-21T16:09:23-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/527eedfaa125a3017f5230a25b6e174386ffea12
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 777ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.772 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.367 s
[INFO] Finished at: 2017-02-21T16:09:28-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 963ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.287 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.012 s
[INFO] Finished at: 2017-02-21T16:09:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/bff42077d309887fb2762e6b3acb7d910d60021d
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 958ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.148 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.759 s
[INFO] Finished at: 2017-02-21T16:09:45-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 2 3 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 88 out of 125 test classes using 974ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 1362, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.48 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 37/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd

Results :

Tests run: 1362, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.801 s
[INFO] Finished at: 2017-02-21T16:10:00-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/257a489c11fd4110c201d1182af4e1530573f20b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 2 3 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 88 out of 125 test classes using 935ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

TestCopticChronology.testCalendar

TestEthiopicChronology.testCalendar

TestIslamicChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 1362, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 11.533 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 37/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 37

Results :

Tests run: 1362, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 16.087 s
[INFO] Finished at: 2017-02-21T16:10:17-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 919ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.238 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.319 s
[INFO] Finished at: 2017-02-21T16:10:28-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/1735445fbdf8829f7ddd5985d26414d98b3c9ccd
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 783ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.911 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.705 s
[INFO] Finished at: 2017-02-21T16:10:34-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 3 0 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0 0 0 0 1 2
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoProvider$1:run:()Ljava/io/InputStream;:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoProvider:openResource:(Ljava/lang/String;)Ljava/io/InputStream;:2
[FaultTracer] RTS excluded 114 out of 125 test classes using 695ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 455, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.066 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 11/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae

Results :

Tests run: 455, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.141 s
[INFO] Finished at: 2017-02-21T16:10:47-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/b286423ba5ebb365f7c65bb9214dad5f4b3670f5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 3 0 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0 0 0 0 1 2
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoProvider$1:run:()Ljava/io/InputStream;:0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoProvider:openResource:(Ljava/lang/String;)Ljava/io/InputStream;:2
[FaultTracer] RTS excluded 114 out of 125 test classes using 729ms 
Running org.joda.time.TestAllPackages

TestBuddhistChronology.testCalendar

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 455, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.85 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 11/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 11

Results :

Tests run: 455, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.438 s
[INFO] Finished at: 2017-02-21T16:10:55-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 828ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.291 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.853 s
[INFO] Finished at: 2017-02-21T16:11:05-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 14 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2acaddc80b7aef2d1a1538a52c32c3b472c347ae
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 760ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.795 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.557 s
[INFO] Finished at: 2017-02-21T16:11:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 705ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.233 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.660 s
[INFO] Finished at: 2017-02-21T16:11:20-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7a1faaf78c957ca7fefb61bffd74ce355f735cb1
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 857ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.894 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.784 s
[INFO] Finished at: 2017-02-21T16:11:26-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 667ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.212 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.390 s
[INFO] Finished at: 2017-02-21T16:11:35-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/569fe0b4cc7db61af8d1b95e2f688b3543f46374
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 944ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.126 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.928 s
[INFO] Finished at: 2017-02-21T16:11:42-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 822ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.266 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.126 s
[INFO] Finished at: 2017-02-21T16:11:53-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ebfdf29f39d4271dfedb5c3c659040f85a0ea5df
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 663ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.786 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.534 s
[INFO] Finished at: 2017-02-21T16:11:58-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 818ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.318 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.588 s
[INFO] Finished at: 2017-02-21T16:12:08-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/2e27ba998265f5d761b63306b7ae0a3f1cdffcbc
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 663ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.696 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.164 s
[INFO] Finished at: 2017-02-21T16:12:13-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 1 0 0 6 0 0 0 0 0 0 0 0 1 0 0 3 0
[FaultTracer] RTS excluded 123 out of 125 test classes using 680ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 75, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.257 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b

Results :

Tests run: 75, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.422 s
[INFO] Finished at: 2017-02-21T16:12:22-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c866452a78b537a4459ccde2693eb0db1aaa08eb
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 4 0 0 0 0 0 0 0 0 1 0 0 6 0 0 0 0 0 0 0 0 1 0 0 3 0
[FaultTracer] RTS excluded 123 out of 125 test classes using 870ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 75, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.064 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 75, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.001 s
[INFO] Finished at: 2017-02-21T16:12:28-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 665ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.236 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.572 s
[INFO] Finished at: 2017-02-21T16:12:37-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5714b9360e324f8bbc47bed424cfe613f82e931b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 708ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.806 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.205 s
[INFO] Finished at: 2017-02-21T16:12:42-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 858ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.29 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.755 s
[INFO] Finished at: 2017-02-21T16:12:51-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/57224c530e407fdaab7b451641acd5b177953dde
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 734ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.727 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.290 s
[INFO] Finished at: 2017-02-21T16:12:56-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 2010ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.67 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.743 s
[INFO] Finished at: 2017-02-21T16:13:22-06:00
[INFO] Final Memory: 36M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/1e86b1eb37c158e6970de3cf9d25bf4ebbaedcb2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1603ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.934 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.491 s
[INFO] Finished at: 2017-02-21T16:13:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1894ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.786 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.633 s
[INFO] Finished at: 2017-02-21T16:14:06-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ac49bf6dc694550eff859159c1ac9fc33ba1b3e7
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1974ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.046 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 13.557 s
[INFO] Finished at: 2017-02-21T16:14:23-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1000ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.323 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.712 s
[INFO] Finished at: 2017-02-21T16:14:44-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/0815c2db3af376598c85336c0bdbb4ecb72439f5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1035ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.131 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.582 s
[INFO] Finished at: 2017-02-21T16:14:51-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 3 0 0 0 1 0 0 0 2 0 0 0 10 1 0 0 1 0 0 0 2 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$TimeZoneId:parseInto:(Lorg/joda/time/format/DateTimeParserBucket;Ljava/lang/CharSequence;I)I:0
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$TimeZoneId:<clinit>:()V:0
[FaultTracer] RTS excluded 123 out of 125 test classes using 970ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 124, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.433 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f

Results :

Tests run: 124, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.511 s
[INFO] Finished at: 2017-02-21T16:15:02-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f054298876a1f37f864eb5ed3fd0eb56134b2965
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 3 0 0 0 1 0 0 0 2 0 0 0 10 1 0 0 1 0 0 0 2 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$TimeZoneId:parseInto:(Lorg/joda/time/format/DateTimeParserBucket;Ljava/lang/CharSequence;I)I:0
[FaultTracer]  Basic block changes: org/joda/time/format/DateTimeFormatterBuilder$TimeZoneId:<clinit>:()V:0
[FaultTracer] RTS excluded 123 out of 125 test classes using 972ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 124, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.298 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 2/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 2

Results :

Tests run: 124, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.117 s
[INFO] Finished at: 2017-02-21T16:15:09-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 941ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.34 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.219 s
[INFO] Finished at: 2017-02-21T16:15:20-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/80c7dedc1cf7ecaf81a206cd90c8f08975ed7d3f
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 713ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.054 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.455 s
[INFO] Finished at: 2017-02-21T16:15:25-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 937ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.294 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.844 s
[INFO] Finished at: 2017-02-21T16:15:36-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5f7609b6c98a8bfef7cadf5ae98501f8debde2e8
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 677ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.696 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.228 s
[INFO] Finished at: 2017-02-21T16:15:41-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 705ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.246 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.460 s
[INFO] Finished at: 2017-02-21T16:15:50-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/63759ede8cce761faa5c55e43a667bfcd7b3639b
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 704ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.708 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.993 s
[INFO] Finished at: 2017-02-21T16:15:55-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 688ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.246 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.790 s
[INFO] Finished at: 2017-02-21T16:16:05-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eb693d4746b0d938a0dd669f1ca20297ecb09990
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 683ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.685 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.029 s
[INFO] Finished at: 2017-02-21T16:16:10-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 686ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.318 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.445 s
[INFO] Finished at: 2017-02-21T16:16:18-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/88555b56eb85e70d8d051228a61186448551dcce
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 751ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.696 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.078 s
[INFO] Finished at: 2017-02-21T16:16:23-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 5 0 0 0 0 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 5 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 667ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.201 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.451 s
[INFO] Finished at: 2017-02-21T16:16:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/836aa412608fea64a9bd66a403637dda15dcb161
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 5 0 0 0 0 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 5 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 707ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.033 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.501 s
[INFO] Finished at: 2017-02-21T16:16:38-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 3 0 0 0 0 0 0 0 0 0 1 0 1 2 0 0 0 0 0 0 0 0 1 0 1 2
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:test:(Ljava/lang/String;Lorg/joda/time/DateTimeZone;)Z:2
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:addTransition:(Ljava/util/ArrayList;Lorg/joda/time/tz/DateTimeZoneBuilder$Transition;)Z:16
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder$Transition:isTransitionFrom:(Lorg/joda/time/tz/DateTimeZoneBuilder$Transition;)Z:2
[FaultTracer] RTS excluded 122 out of 125 test classes using 668ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 138, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.354 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 3/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233

Results :

Tests run: 138, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.709 s
[INFO] Finished at: 2017-02-21T16:16:47-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/381cca4018848c2fa75ae93efd44872c3d60cd59
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 3 0 0 0 0 0 0 0 0 0 1 0 1 2 0 0 0 0 0 0 0 0 1 0 1 2
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:test:(Ljava/lang/String;Lorg/joda/time/DateTimeZone;)Z:2
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder:addTransition:(Ljava/util/ArrayList;Lorg/joda/time/tz/DateTimeZoneBuilder$Transition;)Z:16
[FaultTracer]  Basic block changes: org/joda/time/tz/DateTimeZoneBuilder$Transition:isTransitionFrom:(Lorg/joda/time/tz/DateTimeZoneBuilder$Transition;)Z:2
[FaultTracer] RTS excluded 122 out of 125 test classes using 727ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 138, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.454 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 3/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 3

Results :

Tests run: 138, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.904 s
[INFO] Finished at: 2017-02-21T16:16:53-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 759ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.216 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.506 s
[INFO] Finished at: 2017-02-21T16:17:02-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.4
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eaaff1d0687fc86a745ca8f3ea0b069b3cf54233
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 832ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.739 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.481 s
[INFO] Finished at: 2017-02-21T16:17:07-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 675ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.262 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.549 s
[INFO] Finished at: 2017-02-21T16:17:16-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/909046aa9aab5fae75e2152fd6ded722a4d5aba2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 681ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.815 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.547 s
[INFO] Finished at: 2017-02-21T16:17:21-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 697ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.223 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.492 s
[INFO] Finished at: 2017-02-21T16:17:30-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/5ef0b60327ab92d74c554ce9f42d091544de23a9
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 718ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.202 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.800 s
[INFO] Finished at: 2017-02-21T16:17:36-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 945ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.251 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.320 s
[INFO] Finished at: 2017-02-21T16:17:45-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/7ed094c8ce1dfa0e01a8c42e2ea4c2982bf4e5b5
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 692ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.679 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.233 s
[INFO] Finished at: 2017-02-21T16:17:51-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 678ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.219 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.261 s
[INFO] Finished at: 2017-02-21T16:17:59-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/d0f77df52f9e047fa88c3ee351f52706d53cf1d2
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 908ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.007 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.882 s
[INFO] Finished at: 2017-02-21T16:18:05-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 682ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.28 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.262 s
[INFO] Finished at: 2017-02-21T16:18:14-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/e6dd4ba04d1e16bbc9c98cfd19f330a15b6fd060
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 695ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.699 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.245 s
[INFO] Finished at: 2017-02-21T16:18:19-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 696ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.289 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.639 s
[INFO] Finished at: 2017-02-21T16:18:28-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/625c06ae84f088233f387fd14be010bde701581c
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 679ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.775 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.040 s
[INFO] Finished at: 2017-02-21T16:18:33-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 907ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.31 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.703 s
[INFO] Finished at: 2017-02-21T16:18:42-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/ca7bec7931d244bf0cd344efe480aa9d9c54db9e
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 784ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.101 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.566 s
[INFO] Finished at: 2017-02-21T16:18:48-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 690ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.263 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.262 s
[INFO] Finished at: 2017-02-21T16:18:56-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/c61c1f3fb68c2bafb6eb6d73f2bdc505624a9e3a
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 875ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.037 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.487 s
[INFO] Finished at: 2017-02-21T16:19:02-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:compile:(Ljava/io/File;[Ljava/io/File;)Ljava/util/Map;:3
[FaultTracer] RTS excluded 124 out of 125 test classes using 753ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.411 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.611 s
[INFO] Finished at: 2017-02-21T16:19:11-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/eda629fffe60c8315f813950e811c21d8879c557
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:compile:(Ljava/io/File;[Ljava/io/File;)Ljava/util/Map;:3
[FaultTracer] RTS excluded 124 out of 125 test classes using 657ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.851 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.093 s
[INFO] Finished at: 2017-02-21T16:19:16-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseYear:(Ljava/lang/String;I)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 682ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.319 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.512 s
[INFO] Finished at: 2017-02-21T16:19:25-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/4d84d89207088ff08fc76d00bf90a56525aa7589
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
[FaultTracer]  Basic block changes: org/joda/time/tz/ZoneInfoCompiler:parseYear:(Ljava/lang/String;I)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 697ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 13, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.851 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 13, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.130 s
[INFO] Finished at: 2017-02-21T16:19:30-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/DateTimeComparator:compare:(Ljava/lang/Object;Ljava/lang/Object;)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 1723ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 44, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.923 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885

Results :

Tests run: 44, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 10.079 s
[INFO] Finished at: 2017-02-21T16:19:46-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/f81758e92881cb18bc1e0a84e2ac64464dd5ba85
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1
[FaultTracer]  Basic block changes: org/joda/time/DateTimeComparator:compare:(Ljava/lang/Object;Ljava/lang/Object;)I:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 1839ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 44, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.287 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 44, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 14.035 s
[INFO] Finished at: 2017-02-21T16:20:04-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 4 0 0 0 1 0 0 0 0 0 0 1 2 9 0 0 1 0 0 0 0 0 0 1 1 4
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneLongText:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormatter:testZoneShortNameNearTransition:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneText:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormatParse_textHalfdayAM_UK:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testGetShortName_berlin:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormatParse_textEraBC_France:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:<clinit>:()V:14
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_halfdayOfDay:()V:0
[FaultTracer] RTS excluded 116 out of 125 test classes using 1868ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
***Coordinated Universal Time
***Eastern Daylight Time
***Japan Standard Time
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 445, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.616 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 9/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac

Results :

Tests run: 445, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.902 s
[INFO] Finished at: 2017-02-21T16:20:35-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/dc437656ff8b825e87d7edf72125dc368b21c885
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 4 0 0 0 1 0 0 0 0 0 0 1 2 9 0 0 1 0 0 0 0 0 0 1 1 4
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneLongText:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormatter:testZoneShortNameNearTransition:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneText:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormatParse_textHalfdayAM_UK:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testGetShortName_berlin:()V:0
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormatParse_textEraBC_France:()V:0
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:<clinit>:()V:14
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_halfdayOfDay:()V:0
[FaultTracer] RTS excluded 116 out of 125 test classes using 1794ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
***Coordinated Universal Time
***Eastern Daylight Time
***Japan Standard Time
Writing zoneinfo files
Writing ZoneInfoMap
Tests run: 445, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.198 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 9/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 9

Results :

Tests run: 445, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 14.485 s
[INFO] Finished at: 2017-02-21T16:20:52-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 667ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.207 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.294 s
[INFO] Finished at: 2017-02-21T16:21:17-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.5
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/fb2063b3f83610f29572b631edaa7e0471d249ac
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 671ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.66 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.274 s
[INFO] Finished at: 2017-02-21T16:21:22-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneLongText:()V:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 685ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 67, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.271 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588

Results :

Tests run: 67, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.565 s
[INFO] Finished at: 2017-02-21T16:21:32-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/9839f89a0c35f24c21cc026ecc5b1664f7707225
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/format/TestDateTimeFormat:testFormat_zoneLongText:()V:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 669ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 67, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.84 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 67, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.163 s
[INFO] Finished at: 2017-02-21T16:21:37-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 0 0 1 1 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/DateTimeZone:forTimeZone:(Ljava/util/TimeZone;)Lorg/joda/time/DateTimeZone;:19
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testForTimeZone_TimeZone:()V:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 709ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 51, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.271 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4

Results :

Tests run: 51, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.322 s
[INFO] Finished at: 2017-02-21T16:21:45-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/262448b391379d053451719f3d14024e7ed9e588
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 2 0 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 0 0 1 1 0 0 1
[FaultTracer]  Basic block changes: org/joda/time/DateTimeZone:forTimeZone:(Ljava/util/TimeZone;)Lorg/joda/time/DateTimeZone;:19
[FaultTracer]  Basic block changes: org/joda/time/TestDateTimeZone:testForTimeZone_TimeZone:()V:0
[FaultTracer] RTS excluded 124 out of 125 test classes using 762ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 51, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.84 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 1/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 1

Results :

Tests run: 51, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.242 s
[INFO] Finished at: 2017-02-21T16:21:50-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d7d1620197aa8dfd0e9462bcec1523156779a3b9/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: true
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 913ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.305 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d7d1620197aa8dfd0e9462bcec1523156779a3b9

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.701 s
[INFO] Finished at: 2017-02-21T16:22:00-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Joda-Time 2.9.6
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources to META-INF
[INFO] Copying 15 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- exec-maven-plugin:1.4.0:java (compile-tzdb) @ joda-time ---
Writing zoneinfo files
Writing ZoneInfoMap
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ joda-time ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 35 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.3:testCompile (default-testCompile) @ joda-time ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ joda-time ---
[INFO] Surefire report directory: /home/lingming/hybrid-rts/subjects/joda-time/d7d1620197aa8dfd0e9462bcec1523156779a3b9/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
[FaultTracer] Project prefix: org/joda/time
[FaultTracer] Coverage level: block-cov
[FaultTracer] Test level: test-class
SUREFIRE-859: [FaultTracer] If Method Coverage, with RunTime Info?: true
[FaultTracer] Hybrid code: 0
[FaultTracer] Hybrid config: false|false|false|false|false|false|false|false|false|false|false|false|
[FaultTracer] Old version location: /home/lingming/hybrid-rts/subjects/joda-time/53922f2a5d3ca0750c22e4ead2194175545598d4
[FaultTracer] Bytecode location to parse checksum: target
[FaultTracer] Execution only config: false
[FaultTracer] Extracted changes: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
[FaultTracer] RTS excluded 125 out of 125 test classes using 1080ms 
Running org.joda.time.TestAllPackages

Testing Gregorian chronology over 1000 iterations
100% complete (i=1000)

Testing Julian chronology over 1000 iterations
100% complete (i=1000)
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.074 sec - in org.joda.time.TestAllPackages
[FaultTracer] Select 0/125 tests for /home/lingming/hybrid-rts/subjects/joda-time/d7d1620197aa8dfd0e9462bcec1523156779a3b9
[FaultTracer] Test coverage set consistent with mvn test set: total 125, reran 0

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.768 s
[INFO] Finished at: 2017-02-21T16:22:06-06:00
[INFO] Final Memory: 26M/1930M
[INFO] ------------------------------------------------------------------------
