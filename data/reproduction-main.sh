#!/bin/bash

### all results will be stored in this dir
RES=fse17-results;
mkdir $RES;

echo "Reproduction start time...$(date)"

# get the raw time data for FRTS and all HRTS variants except HRTSb
./rtsResParse-time.sh > $RES/rts-time.dat

echo "Finishing time data for all except HRTSb...$(date)"

# get the raw test number data for FRTS and all HRTS variants except HRTSb
./rtsResParse-testNum.sh > $RES/rts-testNum.dat

echo "Finishing test number data for all except HRTSb...$(date)"

# get the raw time data for HRTSb
./rtsResParse-time-block.sh > $RES/rts-time-block.dat

echo "Finishing time data for HRTSb...$(date)"

# get the raw test number data for HRTSb
./rtsResParse-testNum-block.sh > $RES/rts-testNum-block.dat

echo "Finishing test number data for HRTSb...$(date)"


#### print the paper tables/figs in dir "fse17-results" ###

# print change statistics (table 4)
./printDiffTable.sh

# print figure 3 in the paper
Rscript 2-way-boxplot.r

# print the study results for test number (table 5)
Rscript testNum-study.r 

# print the study results for AE time (table 6)
Rscript time-study.r 

# print HRTSf and HRTSb results (table 7)
Rscript refinedTableComp.r


echo "Reproduction end time...$(date)"
