#!/bin/bash

DIFF=" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
JDKDIFF=" 0 23 1046 *"
# consider all versions or only changed ones
RANGE=$1
ClassCode=14
RTSRES=rts.res

# read raw rts.res file
readFile () {
v=-1;
while read line; do 
    
    if [[ $line == \[FaultTracer\]\ Hybrid\ code:* ]]; then
	LEVEL="${line##* }"; 
    fi
    if [[ $line == \[FaultTracer\]\ Old\ version\ location:* ]]; then
	OLDV="${line##* }";
	#echo $OLDV
    fi
    if [[ $line == \[FaultTracer\]\ Coverage\ level:* ]]; then
	COV=${line##* };
	#echo $COV
    fi
    if [[ $line == \[FaultTracer\]\ Execution\ only\ config:* ]]; then
	EXEONLY=${line##* };
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Extracted\ changes:* ]]; then
	CHANGES=${line##*:};
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Select\ * ]]; then
	#echo "|"$CHANGES"|"$DIFF"|"
	if [[ $RANGE != "all" ]] && [[ $CHANGES == $DIFF ]]; then
	    #echo $CHANGES;
	    continue;
	fi
	IFS=' ' read -a items<<< $line
	TEST=${items[2]};
	#echo $TIME
	if [ $OLDV != . ]; then
	    if [ $COV == meth-cov -a $LEVEL == 0 ]; then
		if [  $EXEONLY == true ]; then
		    let v++;
		fi
		for ((l=0;l<ClassCode;l++)); do
		    cell=matrix$v$EXEONLY[$l]
		    eval "$cell=$TEST"
		done
	    fi
	    if [[ $COV == meth-cov* ]] && [[ $LEVEL != 0 ]]; then
		cell=matrix$v$EXEONLY[$LEVEL]
		eval "$cell=$TEST"
            fi
	    if [ $COV == class-cov ]; then
		cell=matrix$v$EXEONLY[$ClassCode]
		eval "$cell=$TEST"
            fi
	fi
    fi
done < $1
}

# transform min to seconds
formatter () {
    selTests=${1%/*};
    allTests=${1#*/};
    TOPRINT=$(echo "scale=2;$selTests*100/$allTests"|bc);
    #TOPRINT=$selTests;
}

# print raw result matrix
printMatrix () {
    echo $SUB
for tag in true false; do
for ((i=0;i<=v;i++))
do
    printf $i" "
    for ((j=0;j<=ClassCode;j++))
    do
	cell=matrix$i$tag[$j]
	formatter ${!cell}
	#printf ${!cell}
	printf $TOPRINT" "
    done
    printf "\n"
done
done
}

#print R data
printRData (){
for tag in true false; do
for ((i=0;i<=v;i++))
do
    for ((j=0;j<=ClassCode;j++))
    do
	cell=matrix$i$tag[$j]
	formatter ${!cell}
	#printf ${!cell}
	printf $1"\t"$i"\t"$tag"\t"$j"\t"$TOPRINT"\n"
    done
done
done
}

# print avg
printAvg () {
    echo $SUB
for tag in true false; do
for((j=0;j<=ClassCode;j++))
do
    avg[$j]=0;
done
for ((i=0;i<=v;i++))
do
    for ((j=0;j<=ClassCode;j++))
    do
	cell=matrix$i$tag[$j]
	formatter ${!cell}
	avg[$j]=$(echo "${avg[$j]} + $TOPRINT"|bc)
    done
done
printf "EXEONLY="$tag" "
for((j=0;j<=ClassCode;j++))
do
    var=avg[$j]
    val=$(echo "scale=2;${!var}/$((v+1))"|bc)
    printf $val" "
done
printf "\n"
done
}



SUBSORG=("invokebinder" "compile-testing" "commons-jxpath" "commons-fileupload" "joda-time" "commons-functor" "la4j" "commons-dbutils" "logstash-logback-encoder" "commons-validator" "asterisk-java" "commons-cli" "commons-email" "jfreechart" "commons-codec" "commons-collections" "commons-compress" "commons-lang" "commons-configuration" "commons-imaging" "commons-net" "java-apns" "commons-dbcp" "log4j" "closure-compiler" "HikariCP" "commons-math" "commons-io" "stream-lib" "OpenTripPlanner" "commons-pool" "mapdb")

SUBS=("mapdb")

printf "Sub\tVer\tTag\tTech\tTestNum\n"
for SUB in ${SUBSORG[@]}
do
#if [ $SUB ==  "mapdb" ]; then
#if [ $SUB == "stream-lib" -o  $SUB == "log4j" -o $SUB == "mapdb" ]; then
#if [ $SUB != "log4j" ]; then
#    continue;
#fi
    readFile $SUB/$RTSRES $SUB
    #printMatrix
    #printAvg;
    printRData $SUB;
#fi
done
