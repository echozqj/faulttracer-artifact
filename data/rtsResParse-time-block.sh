#!/bin/bash

DIFF=" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
JDKDIFF=" 0 23 1046 *"
# consider all versions or only changed ones
RANGE=$1
RTSRES=rts-block.res

# read raw rts.res file
readFile () {
v=-1;
while read line; do 
   
    if [[ $line == \[FaultTracer\]\ Old\ version\ location:* ]]; then
	OLDV="${line##* }";
	#echo $OLDV
    fi
    if [[ $line == \[FaultTracer\]\ Execution\ only\ config:* ]]; then
	EXEONLY=${line##* };
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Extracted\ changes:* ]]; then
	CHANGES=${line##*:};
	#echo $EXEONLY
    fi
    if [[ $line == \[INFO\]\ Total\ time:* ]]; then
	#echo "|"$CHANGES"|"$DIFF"|"
	if [[ $RANGE != "all" ]] && [[ $CHANGES == $DIFF ]]; then
	    #echo $CHANGES;
	    continue;
	fi
	TIME=$(echo ${line#*:}| sed "s/[[:space:]]//g");
	#echo $TIME
	if [ $OLDV != . ]; then
		if [  $EXEONLY == true ]; then
		    let v++;
		fi
		eval "blockRes$EXEONLY[$v]=$TIME";
	fi
    fi
done < $1
}

# transform min to seconds                                                                                                                                                
minToSec () {
    #printf $1"\n"                                                                                                                                                         
    if [[ $1 == *s ]]; then
        TOPRINT=${1%s*};
    elif [[ $1 == *min ]]; then
        TRIM=${1%m*};
        MIN=${TRIM%:*};
        SEC=${TRIM#*:};
        SEC=${SEC#0};
        MIN=${MIN#0};
        TOPRINT=$((MIN*60+SEC))
    elif [[ $1 == *h ]]; then
        TRIM=${1%h*};
        HOUR=${TRIM%:*};
        MIN=${TRIM#*:};
        MIN=${MIN#0};
        HOUR=${HOUR#0};
        TOPRINT=$((HOUR*3600+MIN*60));
    fi
}

# print raw result matrix
printMatrix () {
    echo $SUB" "
for ((i=0;i<=v;i++))
do
    printf $i" "
for tag in true false; do
    cell=blockRes$tag[$i];
    minToSec ${!cell};
    printf $TOPRINT" ";
done
    printf "\n"
done
}

#print R data
printRData (){
for ((i=0;i<=v;i++))
do
    for tag in true false; 
    do
	cell=blockRes$tag[$i];
	minToSec ${!cell};
	#printf ${!cell}
	RATIO=$(echo "scale=2;$TOPRINT*100/$avgTime"|bc)
        printf $1"\t"$i"\t"$tag"\t"15"\t"$TOPRINT"\t"$RATIO"\n"
    done
done
}

# print avg
printAvg () {
    echo $SUB" "
avgtrue=0;
avgfalse=0;
for ((i=0;i<=v;i++))
do
    for tag in true false; do
    cell=blockRes$tag[$i];
    minToSec ${!cell};
    cell=avg$tag;
    val=$(echo "${!cell}+$TOPRINT"|bc);
    eval "avg$tag=$val";
    done
done
for tag in true false; do
    cell=avg$tag;
    printf $(echo "scale=2;${!cell}/$((v+1))"|bc)" ";
done
printf "\n"
}

getOrigTestTime () {
TESTLOG="ftracer-test.log";
avgTime=0;
count=0;
while read line; do
    time=$(cat $SUB/$line/$TESTLOG | grep "\[INFO\] Total time: " | sed "s|\[INFO\] Total time: ||" | sed "s/[[:space:]]//g");
    minToSec $time
    avgTime=$avgTime"+"$TOPRINT;
    let count++;
done < $1
avgTime=$(echo $avgTime | bc);
avgTime=$(echo "scale=2;$avgTime/$count"|bc);
}

SUBSORG=("invokebinder" "compile-testing" "commons-jxpath" "commons-fileupload" "joda-time" "commons-functor" "la4j" "commons-dbutils" "logstash-logback-encoder" "commons-validator" "asterisk-java" "commons-cli" "commons-email" "jfreechart" "commons-codec" "commons-collections" "commons-compress" "commons-lang" "commons-configuration" "commons-imaging" "commons-net" "java-apns" "commons-dbcp" "log4j" "closure-compiler" "HikariCP" "commons-math" "commons-io" "stream-lib" "OpenTripPlanner" "commons-pool" "mapdb")

SUBS=("mapdb")

printf "Sub\tVer\tTag\tTech\tTime\tRatio\n"
for SUB in ${SUBSORG[@]}
do
#if [ $SUB ==  "mapdb" ]; then
#if [ $SUB == "stream-lib" -o  $SUB == "log4j" -o $SUB == "mapdb" ]; then
#if [ $SUB != "la4j" ]; then
#    continue;
#fi
    getOrigTestTime <(cat $SUB/SHAs-TestPass.txt | tail -n +2);
    readFile $SUB/$RTSRES $SUB
    #printMatrix
    #printAvg;
    printRData $SUB;
#fi
done
