#!/bin/bash

DIFF=" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
JDKDIFF=" 0 23 1046 *"
# consider all versions or only changed ones
RANGE=$1
ClassCode=14
RTSRES=rts.res

# read raw rts.res file
readFile () {
v=-1;
while read line; do 
    
    if [[ $line == \[FaultTracer\]\ Hybrid\ code:* ]]; then
	LEVEL="${line##* }"; 
    fi
    if [[ $line == \[FaultTracer\]\ Old\ version\ location:* ]]; then
	OLDV="${line##* }";
	#echo $OLDV
    fi
    if [[ $line == \[FaultTracer\]\ Coverage\ level:* ]]; then
	COV=${line##* };
	#echo $COV
    fi
    if [[ $line == \[FaultTracer\]\ Execution\ only\ config:* ]]; then
	EXEONLY=${line##* };
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Extracted\ changes:* ]]; then
	CHANGES=${line##*:};
	#echo $EXEONLY
    fi
    if [[ $line == \[INFO\]\ Total\ time:* ]]; then
	#echo "|"$CHANGES"|"$DIFF"|"
	if [[ $RANGE != "all" ]] && [[ $CHANGES == $DIFF ]]; then
	    #echo $CHANGES;
	    continue;
	fi
	#if [[ $CHANGES == $JDKDIFF ]]; then
	#    continue;
	#fi
	TIME=$(echo ${line#*:}| sed "s/[[:space:]]//g");
	#echo $TIME
	if [ $OLDV != . ]; then
	    if [ $COV == meth-cov -a $LEVEL == 0 ]; then
		if [  $EXEONLY == true ]; then
		    let v++;
		fi
		for ((l=0;l<ClassCode;l++)); do
		    cell=matrix$v$EXEONLY[$l]
		    eval "$cell=$TIME"
		done
	    fi
	    if [[ $COV == meth-cov* ]] && [[ $LEVEL != 0 ]]; then
		cell=matrix$v$EXEONLY[$LEVEL]
		eval "$cell=$TIME"
            fi
	    if [ $COV == class-cov ]; then
		cell=matrix$v$EXEONLY[$ClassCode]
		eval "$cell=$TIME"
            fi
	fi
    fi
done < $1
}

# transform min to seconds
minToSec () {
    #printf $1"\n"
    if [[ $1 == *s ]]; then
	TOPRINT=${1%s*};
    elif [[ $1 == *min ]]; then
	TRIM=${1%m*};
	MIN=${TRIM%:*};
	SEC=${TRIM#*:};
	SEC=${SEC#0};
	MIN=${MIN#0};
	TOPRINT=$((MIN*60+SEC))
    elif [[ $1 == *h ]]; then
	TRIM=${1%h*};
        HOUR=${TRIM%:*};
        MIN=${TRIM#*:};
        MIN=${MIN#0};
	HOUR=${HOUR#0};
	TOPRINT=$((HOUR*3600+MIN*60));
    fi
}

# print raw result matrix
printMatrix () {
echo $SUB
for tag in true false; do
for ((i=0;i<=v;i++))
do
    printf $i" "
    for ((j=0;j<=ClassCode;j++))
    do
	cell=matrix$i$tag[$j]
	minToSec ${!cell}
	#printf ${!cell}
	printf $TOPRINT" "
    done
    printf "\n"
done
done
}

# print R data
printRData () {
for tag in true false; do
for ((i=0;i<=v;i++))
do
    for ((j=0;j<=ClassCode;j++))
    do
        cell=matrix$i$tag[$j]
        minToSec ${!cell}
        #printf ${!cell}
        RATIO=$(echo "scale=2;$TOPRINT*100/$avgTime"|bc)
        printf $1"\t"$i"\t"$tag"\t"$j"\t"$TOPRINT"\t"$RATIO"\n"
    done
#    printf "\n"
done
done
}

# print avg
printAvg () {
echo $SUB
for tag in true false; do
for((j=0;j<=ClassCode;j++))
do
    avg[$j]=0;
done
for ((i=0;i<=v;i++))
do
    for ((j=0;j<=ClassCode;j++))
    do
	cell=matrix$i$tag[$j]
	minToSec ${!cell}
	avg[$j]=$(echo "${avg[$j]} + $TOPRINT"|bc)
    done
done
printf "EXEONLY="$tag" "
for((j=0;j<=ClassCode;j++))
do
    var=avg[$j]
    val=$(echo "scale=2;${!var}/$((v+1))"|bc)
    printf $val" "
done
printf "\n"
done
}

getOrigTestTime () {
TESTLOG="ftracer-test.log";
avgTime=0;
count=0;
while read line; do
    time=$(cat $SUB/$line/$TESTLOG | grep "\[INFO\] Total time: " | sed "s|\[INFO\] Total time: ||" | sed "s/[[:space:]]//g");
    minToSec $time
    #echo $time">>"$TOPRINT                                                                                                                                                            
    avgTime=$avgTime"+"$TOPRINT;
    let count++;
done < $1
avgTime=$(echo $avgTime | bc);
avgTime=$(echo "scale=2;$avgTime/$count"|bc);
#echo $avgTime;                                                                                                                                                                       
}

SUBSORG=("invokebinder" "compile-testing" "commons-jxpath" "commons-fileupload" "joda-time" "commons-functor" "la4j" "commons-dbutils" "logstash-logback-encoder" "commons-validator" "asterisk-java" "commons-cli" "commons-email" "jfreechart" "commons-codec" "commons-collections" "commons-compress" "commons-lang" "commons-configuration" "commons-imaging" "commons-net" "java-apns" "commons-dbcp" "log4j" "closure-compiler" "HikariCP" "commons-math" "commons-io" "stream-lib" "OpenTripPlanner" "commons-pool" "mapdb")

SUBS=("mapdb")

printf "Sub\tVer\tTag\tTech\tTime\tRatio\n"
for SUB in ${SUBSORG[@]}
do
#if [ $SUB ==  "mapdb" ]; then
#if [ $SUB == "stream-lib" -o  $SUB == "log4j" -o $SUB == "mapdb" ]; then
#if [ $SUB == "commons-io" ]; then
#    break;
#fi
    #echo $SUB
    getOrigTestTime <(cat $SUB/SHAs-TestPass.txt | tail -n +2);
    readFile $SUB/$RTSRES $SUB
    printRData $SUB;
    #printMatrix
    #printAvg;
#fi
done
