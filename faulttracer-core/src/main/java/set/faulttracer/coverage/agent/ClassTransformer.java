/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import set.faulttracer.coverage.core.ClassTransformVisitor;
import set.faulttracer.coverage.core.CoverageData;
import set.faulttracer.utils.Properties;

public class ClassTransformer implements ClassFileTransformer
{
	static Set<String> excludedPrefixes = new HashSet<String>();
	static {
		// excludedPrefixes.add("org/junit"); // exclude junit
		// excludedPrefixes.add("junit/"); // exclude junit
		// excludedPrefixes.add("org/apache/maven"); // exclude build system
		excludedPrefixes.add("set/faulttracer"); // exclude itself
		excludedPrefixes.add("org/objectweb"); // exclude its libs
	}

	private static Logger logger = Logger.getLogger(ClassTransformer.class);

	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			if (className == null) {
				return classfileBuffer;
			}
			if (loader != ClassLoader.getSystemClassLoader()) {
				return classfileBuffer;
			}

			if (isExcluded(className)) {
				return classfileBuffer;
			}
			logger.debug("transforming: " + className);

			int clazzId = CoverageData.registerClass(className);
			byte[] result = classfileBuffer;
			ClassReader reader = new ClassReader(classfileBuffer);
			// ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
			// using the following code to fix the issue for linkage errors.
			// TODO: 0.3s slower for joda-time
			ClassWriter writer = new ComputeClassWriter(
					FrameOptions.pickFlags(classfileBuffer));
			ClassTransformVisitor cv = new ClassTransformVisitor(clazzId,
					className.replace("/", "."), writer);
			reader.accept(cv, ClassReader.EXPAND_FRAMES);
			result = writer.toByteArray();
			return result;
		} catch (Throwable t) {
			t.printStackTrace();
			String message = "Exception thrown during instrumentation";
			logger.error(message, t);
			System.err.println(message);
			System.exit(1);
		}
		throw new RuntimeException("Should not be reached");
	}

	/**
	 * Check if the class should be excluded
	 * 
	 * @param className
	 * @return
	 */
	public boolean isExcluded(String className) {
		if (!Properties.SCOPE_WITH_LIB) {
			if (!className.startsWith(Properties.PROJECT_PREFIX)) {
				return true;
			}
		} else {
			for (String prefix : excludedPrefixes) {
				if (className.startsWith(prefix))
					return true;
			}
		}
		return false;
	}

}
