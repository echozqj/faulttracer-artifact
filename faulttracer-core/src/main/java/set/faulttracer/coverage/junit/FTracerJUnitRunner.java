/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.junit;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.runner.Description;

import com.google.common.io.Files;

import set.faulttracer.coverage.core.CoverageData;
import set.faulttracer.coverage.io.TracerIO;
import set.faulttracer.utils.Properties;

public class FTracerJUnitRunner
{
	static final String className = "set/faulttracer/coverage/junit/FTracerJUnitRunner";
	static final String transJUnit4Runner = "transformJUnit4Runner";
	static final String transJUnit3Runner = "transformJUnit3Runner";
	static final String isExcluded = "isExcluded";
	static final String dumpCov = "dumpCoverage";
	public static Set<String> allTests = new HashSet<String>();
	public static Set<String> runnedTests = new HashSet<String>();
	public static Set<String> excluded = new HashSet<String>();

	public static boolean isExcluded(String testClass) throws IOException {
		// if testClass is abstract higher level runner, should run it;
		// otherwise no lower-level test will be runned
		if (testClass == null)
			return false;
		// first store the test class
		allTests.add(testClass);
		// second check if it should be excluded
		boolean isExcluded = excluded.contains(testClass);
		if (isExcluded) {
			File oldCovFile = new File(Properties.OLD_DIR + File.separator
					+ Properties.TRACER_ROOT_DIR + File.separator
					+ Properties.TRACER_COV_TYPE + File.separator + testClass
					+ Properties.GZ_EXT);
			File newCovFile = new File(Properties.TRACER_ROOT_DIR
					+ File.separator + Properties.TRACER_COV_TYPE
					+ File.separator + testClass + Properties.GZ_EXT);
			newCovFile.getParentFile().mkdirs();
			// bug fix: changing from getAbsolutePath() into getCanonicalPath()
			// to remove redundant names such as "."
			if (!oldCovFile.getCanonicalPath()
					.equals(newCovFile.getCanonicalPath())) {
				Files.copy(oldCovFile, newCovFile);
			}
		} else {
			runnedTests.add(testClass);
		}
		return isExcluded;
	}

	public static void transformJUnit3Runner(String testClass) {
		try {
			dumpCoverage(testClass);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void transformJUnit4Runner(String testClass) {
		try {
			dumpCoverage(testClass);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void dumpCoverage(Description d) throws IOException {
		String test = d.getClassName() + "." + d.getMethodName();
		dumpCoverage(test);
	}

	private static void dumpCoverage(String test) throws IOException {
		if (Properties.EXECUTION_ONLY || test == null
				|| FTracerJUnitRunner.excluded.contains(test))
			return;
		String fileName = Properties.NEW_DIR + File.separator
				+ Properties.TRACER_ROOT_DIR + File.separator
				+ Properties.TRACER_COV_TYPE + File.separator
				+ getFileName(test) + Properties.GZ_EXT;
		File file = new File(fileName);
		file.getParentFile().mkdirs();
		if (Properties.TRACER_COV_TYPE.startsWith(Properties.METH_COV)) {
			if (!CoverageData.WITH_RTINFO)
				TracerIO.writeMethodCov(test, fileName);
			else
				TracerIO.writeMethodCovWithRT(test, fileName);
		} else if (Properties.CLASS_COV.equals(Properties.TRACER_COV_TYPE))
			TracerIO.writeClassCov(test, fileName);
		else if (Properties.STMT_COV.equals(Properties.TRACER_COV_TYPE))
			TracerIO.writeStmtCov(test, fileName);
		else if (Properties.BRANCH_COV.equals(Properties.TRACER_COV_TYPE))
			TracerIO.writeBranchCov(test, fileName);
		else if (Properties.BLK_COV.equals(Properties.TRACER_COV_TYPE))
			TracerIO.writeBlockCovWithRT(test, fileName);
	}

	public static String getFileName(String test) {
		if (Properties.TC_LEVEL.equals(Properties.TEST_LEVEL))
			return test;
		else
			return org.apache.commons.codec.digest.DigestUtils.sha1Hex(test);
	}

}
