/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.junit;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.faulttracer.coverage.core.CoverageData;
import set.faulttracer.utils.Properties;

class JUnit3MethodVisitor extends MethodVisitor implements Opcodes
{

	public JUnit3MethodVisitor(final MethodVisitor mv) {
		super(ASM5, mv);
	}

	@Override
	public void visitCode() {
		// if the test is excluded, then directly return without execution
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD,
				JUnitTestClassLevelTransformer.JUNIT38_RUNNER_CLASS, "fName",
				"Ljava/lang/String;");
		mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
				FTracerJUnitRunner.isExcluded, "(Ljava/lang/String;)Z", false);
		Label label = new Label();
		mv.visitJumpInsn(IFEQ, label);
		mv.visitInsn(RETURN);
		mv.visitLabel(label);
		// mv.visitInsn();
		mv.visitCode();
	}

	// transform the run invocation to dump coverage info
	@Override
	public void visitInsn(int opcode) {
		if (opcode == RETURN) {
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD,
					JUnitTestClassLevelTransformer.JUNIT38_RUNNER_CLASS,
					"fName", "Ljava/lang/String;");
			mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
					FTracerJUnitRunner.transJUnit3Runner,
					"(Ljava/lang/String;)V", false);
		}
		mv.visitInsn(opcode);
	}

}