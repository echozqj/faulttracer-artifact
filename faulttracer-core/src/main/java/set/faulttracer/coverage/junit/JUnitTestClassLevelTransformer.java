/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.junit;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.faulttracer.coverage.core.CoverageData;
import set.faulttracer.utils.Properties;

public class JUnitTestClassLevelTransformer
		implements ClassFileTransformer, Opcodes
{

	private static Logger logger = Logger
			.getLogger(JUnitTestClassLevelTransformer.class);
	// handle junit4 test class runnere
	public static final String PARENT_RUNNER_CLASS = "org/junit/runners/ParentRunner";
	// handle junit3 test class runner
	public static final String JUNIT38_RUNNER_CLASS = "junit/framework/TestSuite";
	// public static final String JUNIT38_RUNNER_CLASS =
	// "junit/framework/TestCase";

	public static final String JUNIT_NOTIFIER_CLASS = "org/junit/runner/notification/RunNotifier";

	public static String className;

	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			if (className == null) {
				return classfileBuffer;
			}
			if (loader != ClassLoader.getSystemClassLoader()) {
				return classfileBuffer;
			}

			if (!className.equals(PARENT_RUNNER_CLASS)
					&& !className.equals(JUNIT38_RUNNER_CLASS)
					&& !className.equals(JUNIT_NOTIFIER_CLASS)) {
				return classfileBuffer;
			}
			logger.debug("transforming JUnit class: " + className);
			this.className = className;

			byte[] result = classfileBuffer;
			ClassReader reader = new ClassReader(classfileBuffer);
			ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

			ClassVisitor cv = new ClassVisitor(ASM5, writer) {
				@Override
				public MethodVisitor visitMethod(final int access,
						final String name, final String desc,
						final String signature, final String[] exceptions) {
					MethodVisitor mv = cv.visitMethod(access, name, desc,
							signature, exceptions);
					// check whether to exclude tests for JUnit4
					if (JUnitTestClassLevelTransformer.className
							.equals(PARENT_RUNNER_CLASS) && name.equals("run"))
						return new JUnit4MethodVisitor(mv);
					// check whether to exclude tests for JUnit3
					else if (JUnitTestClassLevelTransformer.className
							.equals(JUNIT38_RUNNER_CLASS) && name.equals("run")
							&& desc.equals("(Ljunit/framework/TestResult;)V")) {
						return new JUnit3MethodVisitor(mv);
					}
					// collect the coverage info
					else if (JUnitTestClassLevelTransformer.className
							.equals(JUNIT_NOTIFIER_CLASS)
							&& (name.equals("fireTestRunStarted")
									|| name.equals("fireTestRunFinished")))
						return new JUnitTestRunStartEndEventMethodVisitor(mv,
								name);
					else
						return mv;
				}
			};
			reader.accept(cv, ClassReader.EXPAND_FRAMES);
			result = writer.toByteArray();
			return result;
		} catch (Throwable t) {
			t.printStackTrace();
			String message = "Exception thrown during instrumentation";
			logger.error(message, t);
			System.err.println(message);
			System.exit(1);
		}
		throw new RuntimeException("Should not be reached");
	}

}
