/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.rts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;

import set.faulttracer.coverage.io.TracerIO;
import set.faulttracer.coverage.junit.FTracerJUnitRunner;
import set.faulttracer.diff.VersionDiff;
import set.faulttracer.utils.Properties;

/**
 * Selecting affected test classes via method-level regression test selection
 * (e.g., FaultTracer and Chianti)
 * 
 * @author lingmingzhang
 *
 */
public class HybridRTS
{
	public final static String CLASS = "set/faulttracer/rts/HybridRTS";
	private static Logger logger = Logger.getLogger(HybridRTS.class);

	public static void main() throws Exception {
		// if(true)return;
		long startTime = System.currentTimeMillis();

		FTracerJUnitRunner.excluded.clear();
		// compute version diff
		VersionDiff.compute(Properties.OLD_DIR, Properties.NEW_DIR, Properties.NEW_CLASSPATH);

		// if old cov is not available, directly rerun all tests
		if (Properties.OLD_DIR == null) {
			System.out.println(Properties.FTRACER_TAG
					+ "No RTS analysis due to no old coverage, but is computing coverage info and checksum info for future RTS...");
			return;
		}

		// obtain old cov info
		Map<String, Set<String>> old_cov_map = TracerIO
				.loadCovFromDirectory(Properties.OLD_DIR);
		// if no prior version cov, directly return without any exclusion
		if (old_cov_map == null)
			return;

		for (String test : old_cov_map.keySet()) {
			// select changed tests
			if (!VersionDiff.changedFiles.contains(test.replace(".", "/"))
					&& !isAffected(old_cov_map.get(test))) {
				FTracerJUnitRunner.excluded.add(test);
			}
		}
		long endTime = System.currentTimeMillis();

		// writeExcluded(excludedTests);
		System.out.println(Properties.FTRACER_TAG + "RTS excluded "
				+ FTracerJUnitRunner.excluded.size() + " out of "
				+ old_cov_map.keySet().size() + " test classes using "
				+ (endTime - startTime) + "ms ");
	}

	public static boolean isAffected(Set<String> testCov) {
		Set<String> classCov;
		if (Properties.CLASS_COV.equals(Properties.TRACER_COV_TYPE))
			classCov = testCov;
		else
			classCov = TC_ClassLevelRTS.preprocessClass(testCov);
		logger.debug("Class dependency: " + classCov);

		// handle class deletions
		for (String c : VersionDiff.deletedFiles) {
			if (classCov.contains(c))
				return true;
		}

		if (Properties.CLASS_COV.equals(Properties.TRACER_COV_TYPE)) {
			// handle class changes
			for (String c : VersionDiff.changedFiles) {
				if (classCov.contains(c))
					return true;
			}
			return false;
		}

		// handle class-head changes
		for (String c : VersionDiff.classHeaderChanges) {
			if (classCov.contains(c))
				return true;
		}

		// check if the transformed class changes are covered
		for (String c : VersionDiff.transformedClassChanges) {
			if (classCov.contains(c))
				return true;
		}

		// handle static initializer changes
		for (String cm : VersionDiff.CSIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle static initializer deletions
		for (String cm : VersionDiff.DSIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle initializer changes
		for (String cm : VersionDiff.CIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle initializer deletions
		for (String cm : VersionDiff.DIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle method changes
		for (String cm : VersionDiff.CIMs) {
			if (testCov.contains(cm))
				return true;
		}
		for (String cm : VersionDiff.CSMs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle static method deletions
		for (String cm : VersionDiff.DSMs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle method lookup changes for instance method deletions or
		// additions
		for (String cm : VersionDiff.LCs) {
			if (testCov.contains(cm))
				return true;
		}
		return false;
	}

}
